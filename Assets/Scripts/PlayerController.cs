﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float speed;
	public float jumpHeight;
	public float dashSpeed;
	public float dashCooldown;
	public static Vector3 playerVelocity;
	private bool canDoubleJump;
	private float dashUsable;
	private Vector3 lastPos;
	private CapsuleCollider capsuleCollider;
	private Vector3 inputs;
	private Rigidbody rb;

	void Start () {
		//Disable mouse cursor within the game
		Cursor.lockState = CursorLockMode.Locked;

		rb = GetComponent<Rigidbody>();
		capsuleCollider = GetComponent<CapsuleCollider>();
	}
	
	void Update () {
		inputs = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
		inputs = transform.TransformDirection(inputs);
		

		if (Input.GetButtonDown("Jump")) {
			if (Physics.CheckSphere(new Vector3 (transform.position.x, capsuleCollider.bounds.extents.y, transform.position.z), capsuleCollider.radius * 0.9f)) {
				rb.AddForce(new Vector3(0f,  1f * Mathf.Sqrt(jumpHeight * -Physics.gravity.y)  - rb.velocity.y, 0f), ForceMode.VelocityChange);
				canDoubleJump = true;
			} else {
				if (canDoubleJump) {
					rb.AddForce(new Vector3(0f,  1f * Mathf.Sqrt(jumpHeight * -Physics.gravity.y)  - rb.velocity.y, 0f), ForceMode.VelocityChange);
					canDoubleJump = false;
				}
			}
		}

		if (Input.GetButtonDown("Dash") && Time.time > dashUsable) {
			dashUsable = Time.time + dashCooldown;
			rb.AddForce(playerVelocity.normalized * dashSpeed, ForceMode.VelocityChange);
		}
		
		//Enable mouse cursor within the game
		if (Input.GetKeyDown("escape")) {
			Cursor.lockState = CursorLockMode.None;
		}
	}

	void FixedUpdate() {
		playerVelocity = (rb.position - lastPos) + inputs * speed;
		lastPos = rb.position;
		
		rb.MovePosition(rb.position + inputs * speed * Time.fixedDeltaTime);
	}
}
