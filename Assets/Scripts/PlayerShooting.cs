﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {
	public Transform bulletSpawn;
	public static Transform staticBulletSpawn;
	public Camera myCamera;
	public float fireRate;
	private float nextFire;

	//Debugging purposes
	private float screenX;
	private float screenY;
	private Ray screenRay;

	void Update() {
		//Debugging purposes - draw a raycast from bulletSpawn to the center of the screen
		screenX = Screen.width / 2;
		screenY = Screen.height / 2;
		screenRay = Camera.main.ScreenPointToRay(new Vector3(screenX, screenY, 0f));
		Debug.DrawRay(bulletSpawn.position, screenRay.direction * Vector3.Distance(bulletSpawn.position, new Vector3(screenX, screenY, 0f)), new Color(1f,0.922f,0.016f,1f));
		
		staticBulletSpawn = bulletSpawn;
		Fire();
	}

	void Fire() {
		GameObject bullet = ObjectPooler.SharedInstance.GetPooledObject();
		if (Input.GetButton("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			if (bullet != null) {
				bullet.transform.position = bulletSpawn.position;
				bullet.transform.rotation = bulletSpawn.rotation;		
				bullet.SetActive(true);
			}
		}
	}
}
