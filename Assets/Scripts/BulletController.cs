﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
	public float bulletSpeed;
	private Rigidbody rb;
	private Ray screenRay;
	
	void OnEnable() {
		screenRay = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0f));
		rb = GetComponent<Rigidbody>();

		transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 360.0f));		
	}

	void FixedUpdate() {
		if (Vector3.Distance(transform.position, PlayerShooting.staticBulletSpawn.position) >= ObjectPooler.SharedInstance.pooledObjects.Count) {
			gameObject.SetActive(false);
		}
		rb.velocity = (screenRay.direction.normalized * bulletSpeed) + PlayerController.playerVelocity * 0.9f;
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag != "Bullet") {
			gameObject.SetActive(false);
		}
	}
}
