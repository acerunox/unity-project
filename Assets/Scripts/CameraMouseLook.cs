﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouseLook : MonoBehaviour {
	public float sensitivity = 1.0f;
	public float smoothing = 2.0f;
	public float tiltAngle;
	private GameObject character;
	private Vector2 mouseLook;
	private Vector2 smoothV;
	// Use this for initialization
	void Start () {
		character = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
		mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
		smoothV.x = Mathf.Lerp(smoothV.x, mouseDelta.x, 1.0f/smoothing);
		smoothV.y = Mathf.Lerp(smoothV.y, mouseDelta.y, 1.0f/smoothing);
		mouseLook += smoothV;
		mouseLook.y = Mathf.Clamp(mouseLook.y, -90.0f, 90.0f);

		transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
		transform.Rotate(0f, 0f, transform.localRotation.z + Input.GetAxis("Horizontal") * -tiltAngle, Space.Self);
		character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
	}
}
